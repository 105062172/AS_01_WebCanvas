在我的小畫家中要更換功能前都要先按一下該功能所代表的圖示

以下是我有做的功能:
1.Basic control tool(鉛筆圖案以及橡皮擦圖案)
2.Text input(打字按鈕)(打字前要先從下方輸入要打得字以及字形和大小，選擇完後在在畫布上選下要貼上的地方)
3.Cursor Icon
4.Refresh button
5.Different brush shape (我只做了圓形和square)
6.Undo  and Redo(!!若畫面上從開始就只有做一個動作，則只能按clear鍵來消除他)


這次的小畫家中，使用了大部分html所提供的函式庫，但可惜upload 圖片並印出的功能還是做不出來，看來是對於javascript的控制還是不夠熟練!